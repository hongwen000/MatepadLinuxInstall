echo "##########################################################"
echo "#                                                        #"
echo "# Linux 安装器 for matepad                               #"
echo "# 主页：https://gitee.com/hongwen000/MatepadLinuxInstall #"
echo "# 版本：2020-02-15-1                                     #"
echo "#                                                        #"
echo "##########################################################"
echo "提示：第一次使用会请求存储权限，用于检测内部存储中的安装包"
# while true; do
#     read -p "是否继续？(yes/no)" yn
#     case $yn in
#         [Yy]* ) break;;
#         [Nn]* ) exit;;
#         * ) echo "请输入yes或no";;
#     esac
# done
termux-setup-storage
storage_path=$HOME/storage/downloads
image_path=$storage_path/linux_image.tar.gz
if [[ -d "$storage_path" ]]; then
  echo "- 获取存储权限成功"
else
  echo "- 没有成功获取存储权限，退出程序"
  exit
fi
if [[ -f "$image_path" ]]; then
  echo "- 检测到Linux镜像，准备安装"
else
    echo "- 未检测到Linux镜像(backup.tar.gz)"
    echo "  请下载, 并用文件管理器将它移动到到下载文件夹下（内部存储/Download）"
    exit
fi
sed -i 's@^\(deb.*stable main\)$@#\1\ndeb https://mirrors.tuna.tsinghua.edu.cn/termux stable main@' $PREFIX/etc/apt/sources.list
apt update && apt upgrade -y
pkg install sed wget openssl-tool proot pigz -y
target_path=$HOME/arch_os
mkdir -p $target_path
tar xvf $image_path -I pigz -C $target_path
file_list="/usr/bin/dumpe2fs"
file_list="${file_list} /usr/bin/gawk-5.0.1"
file_list="${file_list} /usr/bin/gawk"
file_list="${file_list} /usr/bin/fsck.ext3"
file_list="${file_list} /usr/bin/fsck.ext2"
file_list="${file_list} /usr/bin/e2mmpstatus"
file_list="${file_list} /usr/bin/e2fsck"
file_list="${file_list} /usr/bin/fsck.ext4"
file_list="${file_list} /usr/bin/mkfs.ext4"
file_list="${file_list} /usr/bin/gunzip"
file_list="${file_list} /usr/bin/ld"
file_list="${file_list} /usr/bin/aarch64-unknown-linux-gnu-gcc"
file_list="${file_list} /usr/bin/mkfs.ext3"
file_list="${file_list} /usr/bin/tune2fs"
file_list="${file_list} /usr/bin/aarch64-unknown-linux-gnu-gcc-ar"
file_list="${file_list} /usr/bin/gcc-ar"
file_list="${file_list} /usr/bin/mkfs.ext2"
file_list="${file_list} /usr/bin/uncompress"
file_list="${file_list} /usr/bin/ld.bfd"
file_list="${file_list} /usr/bin/aarch64-unknown-linux-gnu-c++"
file_list="${file_list} /usr/bin/aarch64-unknown-linux-gnu-g++"
file_list="${file_list} /usr/bin/aarch64-unknown-linux-gnu-gcc-9.2.0"
file_list="${file_list} /usr/bin/aarch64-unknown-linux-gnu-gcc-nm"
file_list="${file_list} /usr/bin/aarch64-unknown-linux-gnu-gcc-ranlib"
file_list="${file_list} /usr/bin/gcc"
file_list="${file_list} /usr/bin/c++"
file_list="${file_list} /usr/bin/mke2fs"
file_list="${file_list} /usr/bin/e2label"
file_list="${file_list} /usr/bin/g++"
file_list="${file_list} /usr/bin/gcc-nm"
file_list="${file_list} /usr/bin/aclocal-1.16"
file_list="${file_list} /usr/bin/automake-1.16"
file_list="${file_list} /usr/bin/gcc-ranlib"
file_list="${file_list} /usr/bin/zipinfo"
file_list="${file_list} /usr/bin/automake"
file_list="${file_list} /usr/bin/aclocal"
file_list="${file_list} /usr/bin/unzip"
file_list="${file_list} /usr/bin/getconf"
file_list="${file_list} /usr/lib/getconf/POSIX_V6_LP64_OFF64"
file_list="${file_list} /usr/lib/getconf/POSIX_V7_LP64_OFF64"
file_list="${file_list} /usr/lib/getconf/XBS5_LP64_OFF64"
file_list="${file_list} /usr/include/com_err.h"
file_list="${file_list} /usr/include/et/com_err.h"
file_list="${file_list} /usr/share/man/man5/ext2.5.gz"
file_list="${file_list} /usr/share/man/man5/ext3.5.gz"
file_list="${file_list} /usr/share/man/man5/ext4.5.gz"
file_list="${file_list} /usr/share/man/man8/e2fsck.8.gz"
file_list="${file_list} /usr/share/man/man8/fsck.ext2.8.gz"
file_list="${file_list} /usr/share/man/man8/fsck.ext3.8.gz"
file_list="${file_list} /usr/share/man/man8/fsck.ext4.8.gz"
file_list="${file_list} /usr/share/man/man8/mke2fs.8.gz"
file_list="${file_list} /usr/share/man/man8/mkfs.ext2.8.gz"
file_list="${file_list} /usr/share/man/man8/mkfs.ext3.8.gz"
file_list="${file_list} /usr/share/man/man8/mkfs.ext4.8.gz"
file_list="${file_list} /root/.not_first_time_run"
for file in ${file_list}
do 
    rm $target_path/arch-fs${file}
done
sed -i "s/fcitx/ibus/g" $target_path/arch-fs/root/.vnc/xstartup
remote_path="https://gitee.com/hongwen000/MatepadLinuxInstall/raw/master"
wget ${remote_path}/stopall.fish -O ${target_path}/arch-fs/root/.config/fish/functions/stopall.fish
chmod +x ${target_path}/arch-fs/root/.config/fish/functions/stopall.fish
wget ${remote_path}/startui -O ${target_path}/arch-fs/usr/bin/startui
chmod +x ${target_path}/arch-fs/usr/bin/startui
start_script=$target_path/start-arch-up.sh
chmod +x $start_script
start_command=$HOME/../usr/bin/startlinux
rm -rf $start_command
ln -s $start_script $start_command
echo "安装完成, 使用startlinux命令进入linux，然后使用startui命令启动图形服务"
