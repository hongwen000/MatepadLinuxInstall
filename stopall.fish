function stopall
    while true
        read -P " 本命令将杀死所有程序，是否继续？(yes/no)" yn
        switch $yn
       	 case yes
       		break
       	 case no
       		return
       	 case '*'
       	     echo "请输入yes或no"
             return
        end
    end
    set no_kill_list ps Xvnc proot fish sshd tmux:
    set still_have_to_kill true
	while true
        set prog_list (ps -A --no-headers -o pid,comm | string trim -l | cut -d " " -f 1,2)
        set length (count $prog_list)
        if $still_have_to_kill
            set still_have_to_kill false
            for i in (seq $length)
                set prog_pid (echo $prog_list[$i] | cut -d " " -f 1)
                set prog_name (echo $prog_list[$i] | cut -d " " -f 2)
                if contains $prog_name $no_kill_list
                    continue
                else
                    if contains $prog_pid $no_kill_pid
                        continue
                    else
                        set still_have_to_kill true
                        echo "killing $prog_pid : $prog_name"
                        kill -9 $prog_pid
                        set -a no_kill_pid $prog_pid
                        break
                    end
                end
            end
        else
            break
        end
    end
	for i in (seq 3)
		rm -rf /tmp/.X$i-lock
		rm -rf /tmp/.X11-unix
		vncserver -kill :$i
	end
	pstree
end
